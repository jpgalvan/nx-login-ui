const express = require('express');
const res = require('express/lib/response');
const router = express.Router();
const axios = require('axios');

const BASE_URL = 'https://login-api-back.herokuapp.com/usuario/';


router.post('/', (req, res) => {

    axios({
        method: 'post',
        url: BASE_URL + 'auth/signin',
        data: req.body
    }).then((response) => {
        res.status(201).send({
            success: true,
            id: response.data.id,
            username: response.data.usuario,
            rol: response.data.rol.id
        });
    }, (err) => {
        res.status(404).send("USERNAME_OR_PASSWORD_WRONG");
    });
});

module.exports = router;