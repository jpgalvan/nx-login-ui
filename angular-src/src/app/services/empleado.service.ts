import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  //private API_SERVER = "http://localhost:3000/empleado";
  private API_SERVER = "empleado";

  constructor(private httpClient: HttpClient) { }

  public getAllEmployee(): Observable<any> {
    return this.httpClient.get(this.API_SERVER);
  }

  public getEmployeById(id): Observable<any> {
    return this.httpClient.get(this.API_SERVER+ `/${id}`);
  }

  public delete(id): Observable<any> {
    return this.httpClient.delete(this.API_SERVER + `/${id}`);
  }

  public post(empleado) : Observable<any> {
    return this.httpClient.post(this.API_SERVER,empleado);
  }

  public put(empleado,id) : Observable<any> {
    return this.httpClient.put(this.API_SERVER+ `/${id}`,empleado);
  }

}
