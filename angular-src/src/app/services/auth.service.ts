import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //private API_SERVER = "http://localhost:3000/usuario";
  private API_SERVER = "usuario";

  constructor(private httpClient: HttpClient) { }

  public auth(usuario): Observable<any> {
    return this.httpClient.post(this.API_SERVER, usuario);
  }

  public storeUserinfo(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public logout() {
    localStorage.clear();
  }

  public loggedIn() {
    return (localStorage.getItem('user') === null) ? false : true;
  }
}
