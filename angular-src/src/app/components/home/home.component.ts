import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title: string = "Sistema"

  leftContent: any = [{ description: 'Menú', isTitle: true }];

  usuario: any = [];
  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('user'));
    this.leftContent.push({ description: 'Inicio', isTitle: false, route: 'home' });
    if (this.usuario.rol == 2) {
      this.leftContent.push({ description: 'Empleado', isTitle: false, route: 'home/empleado' });
    }
    else if (this.usuario.rol == 3) {
      this.leftContent.push({ description: 'Jugador', isTitle: false, route: 'home/jugador' });
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['']);
  }
}
