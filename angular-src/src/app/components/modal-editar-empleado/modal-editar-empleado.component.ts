import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmpleadoService } from 'src/app/services/empleado.service';
export interface Empleado {
  id: number,
  brm: string,
  nombre: string,
  puesto: string,
  imagen: string,
  base64: string
}

@Component({
  selector: 'app-modal-editar-empleado',
  templateUrl: './modal-editar-empleado.component.html',
  styleUrls: ['./modal-editar-empleado.component.scss']
})
export class ModalEditarEmpleadoComponent implements OnInit {
  base64: any;
  imgEdit: any;
  empleadoEdit: FormGroup;
  nameImg: string;

  constructor(public dialogRef: MatDialogRef<ModalEditarEmpleadoComponent>,
    public empleadoService: EmpleadoService,
    public fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: Empleado) { }

  ngOnInit() {
    let puesto = (this.data.puesto === 'Desarrollador FrontEnd Sr') ? 1 : 2 ;
    this.empleadoEdit = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
      puesto: ['', Validators.required],
      imagen: [''],
      base64: ['']
    })
    this.empleadoEdit.setValue({
      id: this.data.id,
      nombre: this.data.nombre,
      puesto: puesto,
      imagen: '',
      base64: this.data.base64
    })
    this.nameImg = this.data.imagen
    this.imgEdit = this.data.base64;
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  handleUploadEdit(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgEdit = reader.result.toString().replace('data:image/jpeg;base64,', '');
    };
  }

  actualizar() {
    let temporal = this.empleadoEdit;
    (temporal.value.imagen != '') ? temporal.value.imagen = temporal.value.imagen.split(/(\\|\/)/g).pop() : temporal.value.imagen = this.nameImg
    temporal.value.base64 = this.imgEdit
    temporal.value.puesto = (temporal.value.puesto == 1) ? "Desarrollador FrontEnd Sr" : "Desarrollador FrontEnd Jr" ;
    this.empleadoEdit = temporal;
    this.empleadoService.put(this.empleadoEdit.value, this.empleadoEdit.value.id).subscribe(
      resp => {
        this.empleadoEdit.reset();
        this.dialogRef.close(this.data.brm);
      },
      error => { console.error(error) }
    )
  }
}
