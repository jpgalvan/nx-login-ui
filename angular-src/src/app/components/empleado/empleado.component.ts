import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { AlertService, ConfirmationService } from '@next/nx-controls-common';
import { ModalEditarEmpleadoComponent } from '../modal-editar-empleado/modal-editar-empleado.component';

export interface Empleado {
  id: number,
  brm: string,
  nombre: string,
  puesto: string,
  imagen: string,
  base64: string
}

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {
  empleadoPost: FormGroup;
  empleadoEdit: FormGroup;
  base64: any;
  imgEdit: any;
  display: any = 'none';
  p: number = 1;
  displayedColumns: string[] = ['brm', 'nombre', 'imagen', 'puesto', 'acciones'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public fb: FormBuilder,
    public empleadoService: EmpleadoService,
    public dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.empleadoPost = this.fb.group({
      brm: ['', Validators.required],
      nombre: ['', Validators.required],
      puesto: ['', Validators.required],
      imagen: ['', Validators.required],
      base64: ['']
    })
    this.empleadoEdit = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
      puesto: ['', Validators.required],
      imagen: [''],
      base64: ['']
    })
    this.bindTable()
  }



  bindTable() {
    this.empleadoService.getAllEmployee().subscribe((res) => {
      this.dataSource = new MatTableDataSource<Empleado>(res);
      this.dataSource.paginator = this.paginator;
    },
      (error) => {
        console.error(error);
      });
  }



  eliminar(empleado: any) {
    this.confirmationService.confirm({
      message: '¿Esta seguro de querer realizar esta acción?',
      lblOkBtn: 'Si',
      lblCancelBtn: 'Cancelar',
      accept: () => {
        this.empleadoService.delete(empleado.id).subscribe((res) => {
          this.alertService.error('Empleado con BRM: ' + empleado.brm + ' creado');
          this.bindTable();
        },
          (error) => {
            console.error(error);
          });
      }
    });
  }

  openModalDialog(empleado:Empleado): void {
    const dialog = this.dialog.open(ModalEditarEmpleadoComponent,{
      width:'430px',
      data: empleado
    });
    dialog.afterClosed().subscribe(res=>{
      if(res!=false && res!=undefined) {
        this.alertService.success('Empleado con BRM: ' + res + ' actualizado');
        this.bindTable();
      }
    });
  }

  guardar() {
    let element = <HTMLInputElement>document.getElementById("saveButton");
    element.disabled = true;
    let temporal = this.empleadoPost;
    temporal.value.imagen = temporal.value.imagen.split(/(\\|\/)/g).pop()
    temporal.value.base64 = this.base64
    temporal.value.puesto = (temporal.value.puesto == 1) ? "Desarrollador FrontEnd Sr" : "Desarrollador FrontEnd Jr" ;
    this.empleadoPost = temporal;
    this.empleadoService.post(this.empleadoPost.value).subscribe(
      resp => {
        this.empleadoPost.reset();
        this.alertService.success('Empleado con BRM: ' + resp.brm + ' creado');
        this.bindTable();
      },
      error => {
        this.alertService.success('El BRM: ' + this.empleadoPost.value.brm + ' ya se encuentra en uso');
        element.disabled = false;
      }
    )
  }


  actionMethod(event: any) {
    event.target.disabled = true;
  }



  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64 = reader.result.toString().replace('data:image/jpeg;base64,', '');
    };
  }

  handleUploadEdit(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgEdit = reader.result.toString().replace('data:image/jpeg;base64,', '');
    };
  }
}
