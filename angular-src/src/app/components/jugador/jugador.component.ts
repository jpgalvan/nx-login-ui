import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.component.html',
  styleUrls: ['./jugador.component.scss']
})
export class JugadorComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private document: Document)  { }

  ngOnInit() {
  }
  redirect() {
    this.document.location.href = 'https://nx-tictactoe.herokuapp.com/';
  }
}
