import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatGridListModule, MatCardModule, MatInputModule, MatButtonModule, MatDialogModule, MatFormFieldModule }from '@angular/material';
import {MatSelectModule} from '@angular/material/select';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonsModule } from '@next/nx-controls-common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RootComponent } from './components/root/root.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { JugadorComponent } from './components/jugador/jugador.component';
import { MaterialModule } from './modules/material/material.module';
import { ModalEditarEmpleadoComponent } from './components/modal-editar-empleado/modal-editar-empleado.component';

@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    LoginComponent,
    HomeComponent,
    EmpleadoComponent,
    JugadorComponent,
    ModalEditarEmpleadoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    CommonsModule.forRoot(),
    MatGridListModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MaterialModule,
    MatSelectModule,
    MatFormFieldModule
  ],
  entryComponents: [ModalEditarEmpleadoComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
